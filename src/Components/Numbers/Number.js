import React from 'react';

import './Number.css';

const Number = (prop) => {
  return (
      <div className="number">
        <span>{prop.num}</span>
      </div>
  );
};

export default Number;