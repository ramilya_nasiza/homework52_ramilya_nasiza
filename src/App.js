import React, { Component } from 'react';
import Number from './Components/Numbers/Number.js';


import './App.css';


class App extends Component {

  state = {
    numbers: []
  };

  randomNumbers = () => {
    let array = [];
    let num;
    while (array.length < 5){
      num =  Math.floor(Math.random() * 32) + 5;
      if (!array.includes(num)){
        array.push(num);
      }
    }
    return array.sort((a, b) => {return a - b});
  };

  changeNumbers = () => {
    let nums = [...this.state.numbers];
    const randNumbers = this.randomNumbers();
    nums = randNumbers;
    this.setState({
      numbers: nums
    });
  };

  render() {
    return (
        <div className="App">
          <div>
            <button onClick={this.changeNumbers}>Change numbers</button>
          </div>
          {this.state.numbers.map((number, index) => {
            return (
                <Number key={index} num={number} />
            )
          })}
        </div>
    );
  }
}


export default App;
